<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    //
    protected $fillable = [
        'id_pengunjung', 'id_karyawan', 'jml_kamar', 'tgl_masuk', 'tgl_keluar', 'lama_nginap', 'total_harga',
    ];
}
