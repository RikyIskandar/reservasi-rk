<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class detail_transaksi extends Model
{
    //
    protected $fillable = [
        'no_transaksi', 'no_kamar',
    ];

    public function transaksis()
    {
        return $this->hasMany(Transaksi::class);

        return $timestamps = true;
    }
}
