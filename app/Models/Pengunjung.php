<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengunjung extends Model
{
    //
    protected $fillable = [
        'Nama_pengunjung', 'Alamat', 'Jenis_Kelamin', 'No_Tlp', 'No_KTP',
    ];
}
