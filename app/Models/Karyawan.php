<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    //
    protected $fillable = [
        'Nama_Karyawan', 'Jenis_Kelamin',
    ];
}
