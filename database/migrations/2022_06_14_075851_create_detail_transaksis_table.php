<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksis', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('no_transaksi');
            $table->unsignedBigInteger('no_kamar');
            $table->timestamps();

            $table->foreign('no_kamar')->references('no_kamar')->on('kamars');
            $table->foreign('no_transaksi')->references('no_transaksi')->on('transaksis');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksis');
    }
};
