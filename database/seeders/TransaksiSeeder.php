<?php

namespace Database\Seeders;

use App\Models\Transaksi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaksi::create([
            'id_pengunjung' => 1,
            'id_karyawan' => 1,
            'jml_kamar' => 1,
            'tgl_masuk' => Carbon::create('2022','6','8'),
            'tgl_keluar' => Carbon::create('2022','6','12'),
            'lama_nginap' => 1,
            'total_harga' => 500000
        ]);
    }
}
