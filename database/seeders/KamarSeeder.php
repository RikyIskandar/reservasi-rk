<?php

namespace Database\Seeders;

use App\Models\kamar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KamarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        kamar::create([
            'jenis_kamar' => 'VIP',
            'harga' => 1000000
        ]);
    }
}
