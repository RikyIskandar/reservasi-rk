<?php

namespace Database\Seeders;

use App\Models\Pengunjung;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PengunjungSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pengunjung::create([
            'Nama_Pengunjung' => 'Holip',
            'Alamat' => 'Palengaan',
            'Jenis_Kelamin' => '1',
            'No_Tlp' => '08767389293',
            'No_KTP' => '356748890'
        ]);
    }
}
