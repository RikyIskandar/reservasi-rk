<?php

namespace Database\Seeders;

use App\Models\detail_transaksi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DetailTransaksiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        detail_transaksi::create([
            'no_transaksi' => 1,
            'no_kamar' => 1,
        ]);
    }
}
