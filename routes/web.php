<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/karyawan',['uses' => 'KaryawanController@index']);
$router->get('/karyawan/{id}',['uses' => 'KaryawanController@show']);

$router->get('/pengunjung',['uses' => 'PengunjungController@index']);
$router->get('/pengunjung/{id}',['uses' => 'PengunjungController@show']);

$router->get('/transaksi',['uses' => 'TransaksiController@index']);
$router->get('/transaksi/{id}',['uses' => 'TransaksiController@show']);

$router->get('/kamar',['uses' => 'KamarController@index']);
$router->get('/kamar/{id}',['uses' => 'KamarController@show']);

$router->get('/detail_transaksi',['uses' => 'DetailTransaksiController@index']);
$router->get('/detail_transaksi/{id}',['uses' => 'DetailTransaksiController@show']);
$router->post('/detail_transaksi', ['uses' => 'DetailTransaksiController@store']);
